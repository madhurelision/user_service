export const LOGIN = 'LOGIN';
export const SUCCESS = "SUCCESS";
export const SUCCESS_REDIRECT = "SUCCESS_REDIRECT";
export const LOADING = "LOADING";
export const FAILED = "FAILED";
export const LOGOUT = "LOGOUT";
export const LOADERCLOSE = "LOADERCLOSE";
export const LIKE_SUCCESS = 'LIKE_SUCCESS'

export const ALL_LOG_LIST = 'ALL_LOG_LIST'
