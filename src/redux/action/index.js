import { adminProfile, loginAdmin, AllLogList } from "../api/index"

import { LOADING, FAILED, SUCCESS, LOGIN, LOGOUT, ALL_LOG_LIST } from "./types"


export const adminLogin = (val) => async dispatch => {
  dispatch({ type: LOADING })
  try {
    const { data } = await loginAdmin(val)
    dispatch({ type: SUCCESS })
    dispatch({ type: LOGIN, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getProfileAdmin = () => async dispatch => {

  try {
    const { data } = await adminProfile()
    dispatch({ type: LOGIN, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    dispatch({ type: LOGOUT })
  }

}

//log
export const getAllLogList = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllLogList(skip, limit)
    dispatch({ type: ALL_LOG_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}