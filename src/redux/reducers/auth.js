import { uniqueElements } from "../../components/config/keyData";
import { FAILED, LOADING, SUCCESS, LOGIN, LOGOUT, LOADERCLOSE, SUCCESS_REDIRECT, LIKE_SUCCESS, ALL_LOG_LIST } from "../action/types";

const initialState = {
  isAuthenticated: false,
  profile: {},
  loader: false,
  open: false,
  close: false,
  created: false,
  updated: false,
  complete: false,
  notify: false,
  message: '',
  logList: [],
  logTotal: null
}

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LOGIN:
      localStorage.setItem('short-token', payload.token)
      localStorage.setItem('isAuthenticated', true)
      return {
        ...state,
        profile: payload.data,
        isAuthenticated: true,
        loader: false,
        open: false,
        close: false,
        created: false,
        updated: false,
      }
    case SUCCESS:
      return {
        ...state,
        loader: false,
        open: true,
        close: false,
        message: payload
      }
    case LOADING:
      return {
        ...state,
        loader: true,
      }
    case FAILED:
      return {
        ...state,
        loader: false,
        open: false,
        close: true,
        updated: false,
        message: payload
      }
    case LOADERCLOSE:
      return {
        ...state,
        loader: false,
        open: false,
        close: false,
        created: false,
        complete: false,
        updated: false,
        notify: false,
        clone: false,
        message: ''
      }
    case SUCCESS_REDIRECT:
      return {
        ...state,
        loader: false,
        open: true,
        close: false,
        created: true,
        complete: false,
        updated: false,
        message: payload,
      }
    case LIKE_SUCCESS:
      return {
        ...state,
        loader: false,
        open: true,
        close: false,
        complete: false,
        updated: true,
        message: payload
      }
    case LOGOUT:
      localStorage.removeItem('short-token')
      localStorage.removeItem('type')
      localStorage.removeItem('isAuthenticated')
      return initialState

    case ALL_LOG_LIST:
      let logRQ = (state.logList.log == null || state.logList.log == undefined) ? {} : uniqueElements(state.logList.log.concat(payload.data.log))
      return {
        ...state,
        logList: state.logList.length == 0 ? payload.data : { log: logRQ },
        logTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        loader: false,
      }
    default:
      return state;
  }
}