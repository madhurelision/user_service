import axios from 'axios';
import { SERVER_URL } from '../../components/config/key';

export const loginAdmin = (data) => axios.post(SERVER_URL + '/login', data);

export const adminProfile = () => axios.get(SERVER_URL + "/profile", {
  headers: {
    'auth-token': localStorage.getItem('short-token')
  }
})

export const AllLogList = (skip, limit) => axios.get(`${SERVER_URL}/getlogList/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('short-token')
  }
})