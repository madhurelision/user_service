import { TablePagination } from '@mui/material'
import React from 'react'

const TablePaginations = ({ page, pages, rowsPerPage, count, handleChangePage, handleChangeRowsPerPage }) => {
  return (
    <TablePagination
      component="div"
      page={page}
      rowsPerPageOptions={pages}
      rowsPerPage={rowsPerPage}
      count={count}
      onPageChange={handleChangePage}
      onRowsPerPageChange={handleChangeRowsPerPage}
    />
  )
}

export default TablePaginations
