import React, { useContext, useEffect, useState } from 'react';
import { IconButton, Grid, AppBar, styled, Toolbar } from '@mui/material'
import { PowerSettingsNew } from '@mui/icons-material';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import { ImageButton, } from '../usableComponent/common';
import { LOGOUT } from '../../redux/action/types';
import { useNavigate } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { Outlet } from 'react-router-dom'

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '100vh',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});


const Dashboard = ({ children }) => {

  const history = useNavigate()
  const dispatch = useDispatch()

  const handleLogout = () => {
    dispatch({ type: LOGOUT })
    history('/')
  }

  return (
    <PageOneWrapper>
      <Grid container direction="column" wrap="nowrap" sx={{ height: '100%' }}>
        <Grid item >
          <AppBar
            variant="outlined"
            position="relative"
            elevation={0}
            sx={{ padding: '8px', backgroundColor: '#242424' }}>
            <Toolbar>
              <Stack direction="row" spacing={3} style={{ width: '100%' }}>
                <Box sx={{ flexGrow: 1 }}>
                  <ImageButton sx={{ verticalAlign: 'sub !important' }} >{"HeyShort"}</ImageButton>
                </Box>
                <Stack
                  direction="row"
                  spacing={3}
                  sx={{ display: { xs: "none", md: "block", sm: "none" } }}
                >
                  <ImageButton onClick={() => { handleLogout() }}>Logout</ImageButton>
                </Stack>

                <Stack
                  direction="row"
                  spacing={3}
                  sx={{ display: { xs: "block", md: "none", sm: "block" } }}
                >
                  <IconButton title="Logout" onClick={handleLogout}>
                    <PowerSettingsNew sx={{ color: '#C31818 !important' }} />
                  </IconButton>
                </Stack>
              </Stack>
            </Toolbar>
          </AppBar>
        </Grid>
        {children}
        <Outlet />
      </Grid>
    </PageOneWrapper>
  )
}

export default Dashboard;
