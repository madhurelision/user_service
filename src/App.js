import React, { useEffect } from 'react';
import './App.css';
import { SuccessSnackbar, FailedSnackbar } from './components/usableComponent/Snackbar'
import Loader from './components/usableComponent/Loader';
import { BrowserRouter as Router, Route, Routes, Navigate, Outlet } from 'react-router-dom';
import Login from './components/Login/Login'
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';
import Dashboard from './components/Dashboard/Dashboard'
import ShortLog from './components/ShortLog/ShortLog';
import { useSelector, useDispatch } from 'react-redux'
import { getProfileAdmin } from './redux/action/index'

function App() {

  const dispatch = useDispatch()
  // show the error or success message
  const openVal = useSelector(state => state.auth.open)
  const closeVal = useSelector(state => state.auth.close)
  const loadVal = useSelector(state => state.auth.loader)
  const user = useSelector(state => state.auth.isAuthenticated)

  useEffect(() => {

    if (localStorage.getItem('short-token') && user == false) {
      dispatch(getProfileAdmin())
    }

  }, [user])

  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/dashboard" element={<ProtectedRoute><Dashboard /></ProtectedRoute>}>
            <Route index element={<ShortLog />} />
          </Route>
        </Routes>
      </Router>
      {openVal && <SuccessSnackbar open={openVal} />}
      {closeVal && <FailedSnackbar open={closeVal} />}
      {loadVal && <Loader />}
    </div>
  );
}

export default App;
